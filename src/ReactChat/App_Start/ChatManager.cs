﻿using ReactChat.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ReactChat.App_Start
{
    public class ChatManager
    {
        private ChatStore chatStore;
        public ChatManager(ChatStore chatStore)
        {
            this.chatStore = chatStore;
        }

        public void AddChat(ChatItem chatItem)
        {
            chatStore.ChatList.Add(chatItem);
        }

        public void AddUser(String userName)
        {
            chatStore.UserList.Add(userName);
        }

        public List<String> GetAllUsers()
        {
            return chatStore.UserList;
        }

        public List<ChatItem> GetAllChat()
        {
            return chatStore.ChatList;
        }
    }
}