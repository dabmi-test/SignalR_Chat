﻿using ReactChat.Controllers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Http.Dependencies;

namespace ReactChat.App_Start
{
    public class WebApiDependencyResolver:IDependencyResolver
    {
        private ChatManager manager;

        public WebApiDependencyResolver(ChatManager chatManager)
        {
            this.manager = chatManager;
        }

        public IDependencyScope BeginScope()
        {
            return this;
        }

        public void Dispose()
        {
            
        }

        public object GetService(Type serviceType)
        {
            return serviceType == typeof(ChatController) ? new ChatController(manager) : null;
        }

        public IEnumerable<object> GetServices(Type serviceType)
        {
            return new Object[0];
        }
    }
}