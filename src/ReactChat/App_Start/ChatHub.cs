﻿using Microsoft.AspNet.SignalR;
using ReactChat.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ReactChat.App_Start
{
    public class ChatHub:Hub
    {
        /// <summary>
        /// Broadcasts the chat message to all the clients.
        /// </summary>
        /// <param name="chatItem"></param>
        public void SendMessage(ChatItem chatItem)
        {
            IHubContext context = GlobalHost.ConnectionManager.GetHubContext(nameof(ChatHub));
            context.Clients.All.pushNewMessage(chatItem.Id, chatItem.UserId, chatItem.UserName, chatItem.Message, chatItem.DateTime);
        }

        /// <summary>
        /// Broadcasts the user list to the clients.
        /// </summary>
        /// <param name="userList"></param>
        public void SendUserList(List<String> userList)
        {
            IHubContext context = GlobalHost.ConnectionManager.GetHubContext(nameof(ChatHub));
            context.Clients.All.pushUserList(userList);
        }
    }
}