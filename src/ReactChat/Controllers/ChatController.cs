﻿using ReactChat.App_Start;
using ReactChat.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace ReactChat.Controllers
{
    public class ChatController : ApiController
    {
        private ChatManager manager;
        private ChatHub chatHub;

        public ChatController(ChatManager chatManager)
        {
            manager = chatManager;
            chatHub = new ChatHub();
        }

        public String GetNewUserId(String userName)
        {
            manager.AddUser(userName);

            chatHub.SendUserList(manager.GetAllUsers());
            return Guid.NewGuid().ToString();
        }

        // GET: api/Chat
        public IEnumerable<ChatItem> Get()
        {
            return manager.GetAllChat();
        }

        // GET: api/Chat/5
        //public string Get(int id)
        //{
        //    return "value";
        //}

        // POST: api/Chat
        public void Post([FromBody]ChatItem chatItem)
        {
            chatItem.Id = Guid.NewGuid();
            chatItem.DateTime = DateTime.Now;
            manager.AddChat(chatItem);

            chatHub.SendMessage(chatItem);
        }

        // PUT: api/Chat/5
        //public void Put(int id, [FromBody]string value)
        //{
        //}

        // DELETE: api/Chat/5
        //public void Delete(int id)
        //{
        //}
    }
}
