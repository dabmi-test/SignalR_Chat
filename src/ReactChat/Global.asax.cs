﻿using ReactChat.App_Start;
using ReactChat.Models;
using System;
using System.Web;
using System.Web.Http;
using System.Web.Mvc;
using System.Web.Optimization;
using System.Web.Routing;

namespace ReactChat
{
    public class MvcApplication : HttpApplication
    {
        protected void Application_Start()
        {
            AreaRegistration.RegisterAllAreas();
            GlobalConfiguration.Configure(WebApiConfig.Register);
            FilterConfig.RegisterGlobalFilters(GlobalFilters.Filters);
            RouteConfig.RegisterRoutes(RouteTable.Routes);
            BundleConfig.RegisterBundles(BundleTable.Bundles);
        }

        protected void Session_Start(object sender, EventArgs e)
        {
            Session["ChatStore"] = new ChatStore();
            ChatManager chatManager = new ChatManager(Session["ChatStore"] as ChatStore);
            GlobalConfiguration.Configuration.DependencyResolver = new WebApiDependencyResolver(chatManager);
        }
    }
}
